// Handle root or redirect to captive portal //
void handleRoot(AsyncWebServerRequest *request) {
  char temp[800];
  snprintf ( temp, 800,
  "<html>\
    <head>\
      <title>%s</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
      <h1>%s</h1>\
      <p>AP Name: %s</p>\ 
      <p>SSID Name: %s</p>\ 
      <p>Version: %s</p>\
      <input type=\"button\" value=\"ON\" onclick=\"setURL('/remote?activate=1')\" />\
      <input type=\"button\" value=\"OFF\" onclick=\"setURL('/remote?activate=0')\" />\
      <p>You may want to <a href='/settings'>configure settings</a>.</p>\
      <script>function setURL(url){document.getElementById('iframe').src = url;}</script>\    
      <iframe id=\"iframe\" style=\"width:0;height:0;border:0; border:none;\" /></iframe>\    
    </body>\
  </html>",
     espname,espname, ap_name, ssid, versionNumber //Passed through variables
  );
  request->send ( 200, "text/html", temp );
}
void handleSettings(AsyncWebServerRequest *request)
{
  char temp[600];
  snprintf ( temp, 600,
  "<html>\
    <head>\
      <title>%s Settings</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
    <h1>Settings</h1>\
    <p><a href='/device_wemo'>Configure Device/WeMo Emulation Settings</a>.</p></br>\
    <p><a href='/wifi'>Configure the Wifi connection</a>.</p></br>\
    <p><a href='/wifiap'>Configure the Wifi AP connection</a>.</p></br>\
    <p><a href='/update'>Do an OTA update</a>.</p>\
    <p>Or you may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>", espname
  );
  request->send ( 200, "text/html", temp );
}

//*Handle OTA Update //
void handleUpdate(AsyncWebServerRequest *request){
  char temp[700];
  snprintf ( temp, 700,
  "<html>\
    <head>\
      <title>%s Updater</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
    <h1>OTA Updater</h1>\
    <p>Arduino IDE Board Setup:</p>\
    <p>Board: Generic ESP8266 Module</p>\
    <p>Flash Size: 1M(64K SPIFFS)</p>\
    <p>Flash Mode: DOUT</p>\
    <p>Reset Method: NODEMCU</p>\
    <p>From the Arduino IDE: Click Sketch-->Export Compiled Binary</p>\
    <form method='POST' action='/updater' enctype='multipart/form-data'><input type='file' name='update' accept='.bin'><input type='submit' value='Update'></form>\
    <p>Or you may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>", espname
  );
  AsyncWebServerResponse *response = request->beginResponse(200, "text/html", temp);
    response->addHeader("Connection", "close");
    response->addHeader("Access-Control-Allow-Origin", "*");
    request->send(response);
}

//* Wifi AP config page handler //

void handleWifiAP(AsyncWebServerRequest *request){
  char temp[850];
  snprintf ( temp, 850,
  "<html>\
    <head>\
      <title>%s WiFi AP</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
      <h1>WIFI AP Config : %s</h1>\
      <p>AP Name: %s</p>\ 
      <p>AP IP: %s</p>\
      <p>SSID Name: %s</p>\ 
      <br />\
      <br /><form method='POST' action='/wifisave'><h4>Configure AP Settings:</h4>\
      <input id='w' name='w' value='2' type='text' hidden>\
      <input type='text' placeholder='Broadcast SSID' name='n'/>\
      <br /><input type='password' placeholder='password' name='p'/>\
      <br /><input type='submit' value='Save'/></form>\
      <p>You may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>",
    espname, espname, ap_name,toStringIp(WiFi.softAPIP()).c_str(), ssid //Passed through variables
  );
  request->send ( 200, "text/html", temp );
}


//* Handle the WLAN save form and redirect to WLAN config page again //
void handleWifiSave(AsyncWebServerRequest *request) {
  int which_wifi;
  which_wifi = request->arg("w").toInt();
  if(which_wifi == 1)
  {
    request->arg("n").toCharArray(ssid, sizeof(ssid) - 1);
    request->arg("p").toCharArray(password, sizeof(password) - 1);
    //server.sendHeader("Location", "/wifi", true);
    saveCredentials();
  }
  else if(which_wifi == 2)
  {
    request->arg("n").toCharArray(ap_name, sizeof(ap_name) - 1);
    request->arg("p").toCharArray(ap_password, sizeof(ap_password) - 1);
    //server.sendHeader("Location", "/wifiap", true);
    saveCredentials();
  }
  else
  {
    //server.sendHeader("Location", "/", true);
  }
  returnHome(request);
}

//* MISC config page handler //
void handleWemo(AsyncWebServerRequest *request) {
char temp[700];
  snprintf ( temp, 700,
  "<html>\
    <head>\
      <title>%s Wemo Setup</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
    <h1>Device/WeMo Config</h1><br />\
    <table><tr><th align='left'>Device Config</th></tr>\
    Current Name [ %s ]</td></tr></table>\
    <br /><form method='POST' action='/wemosave'><h4>Configure Settings:</h4>\
    <input type='text' placeholder='Device/WeMo Emulated Name' name='wemoname'/>\
    <br /><input type='submit' value='Save'/></form>\
      <p>You may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>", espname,espname
  );
  request->send ( 200, "text/html", temp );
}

//Handle WeMo save
void handleWemoSave(AsyncWebServerRequest *request)
{
  request->arg("wemoname").toCharArray(espname, sizeof(espname) - 1);
  saveCredentials();
  returnHome(request);
}

void returnHome(AsyncWebServerRequest *request)
{
  char temp[300];
  snprintf ( temp, 300,
  "<html>\
    <head>\
      <title>%s</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
      <p>You may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>", espname
  );
  request->send ( 200, "text/html", temp );
}

//* Wifi config page handler //
void handleWifi(AsyncWebServerRequest *request){
  int n = WiFi.scanNetworks();
  String wifiNetworks = "";
  if (n > 0) {
    for (int i = 0; i < n; i++) {
      wifiNetworks += (String() + "\r\n<tr><td>" + WiFi.SSID(i) + String((WiFi.encryptionType(i) == ENC_TYPE_NONE)?" ":" *") + " (" + WiFi.RSSI(i) + ")</td></tr>");
    }
  } else {
    wifiNetworks = (String() + "<tr><td>No WLAN found</td></tr>");
  }
  char temp[1500];
  snprintf ( temp, 1500,
  "<html>\
    <head>\
      <title>%s WiFi Config</title>\
      <style>\
        body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
      </style>\
    </head>\
    <body>\
      <h1>WIFI Config : %s</h1>\
      <p>SSID Name: %s</p>\ 
      <p>IP Address: %s</p>\
      <br />\
      <br /><form method='POST' action='/wifisave'><h4>Connect to network:</h4>\
      <input id='w' name='w' value='1' type='text' hidden>\
      <input type='text' placeholder='Broadcast SSID' name='n'/>\
      <br /><input type='password' placeholder='password' name='p'/>\
      <br /><input type='submit' value='Connect'/></form>\      
      <br />\
      <table><tr><th align='left'>WLAN list (refresh if any missing)</th></tr>\
      %s\
      <p>You may want to <a href='/'>return to the home page</a>.</p>\
    </body>\
  </html>",
     espname, espname, ssid,toStringIp(WiFi.localIP()).c_str(), wifiNetworks.c_str() //Passed through variables
  );
  request->send ( 200, "text/html", temp );
}


void handleArgs(AsyncWebServerRequest *request)
{
  String message = "";
  if(request->arg("activate") != "")
  {
    message += "|activate: ";
    if(request->arg("activate") == "1")
    {
      setState(HIGH);
      message += "ON";
    }
    else if(request->arg("activate") == "0")
    {
      setState(LOW);
      message += "OFF";
    }
  }
  if(message != "")
  {
    Serial.print(message);
    handleRoot(request);
  }
  else
  {
    //handleNotFound();
  }
}
