# README #
This project is my own creation of different sources to be able to flash and control the SONOFF modules the way I wanted to. 
The current project as of version 1.6 has the baseline of what I wanted to be able to do with one of these modules:
	Create an Access Point at startup, disabling only when it can connect to another network
	Allow you to modify the settings and store in EEPROM
	Allow for upgrading through an HTML page
		Change the AP name/password
		Change the SSID name/password
		Change the Alexa Integration name
	Allow for control through an HTML page (on/off)
	Have Alexa integration, to be able to tell it to turn X on/off
	Be able to hit a web address on the module to turn it on/off	


# To Flash #

   For the SONOFF, use 1M with 64k spiffs
   Pinout: Button-3.3v-RX-TX-GND-GPIO14
   To flash using a Wemos D1....
      Connect up the 3.3v and GND, Connect RX to RX and TX to TX. Then put a jumper on GND and RESET on the Wemos
      To program, simply hold down the button on the SONOFF and power on the Wemos, flash as normal
