
/** IP to String? */
String toStringIp(IPAddress ip) {
  String res = "";
  for (int i = 0; i < 3; i++) {
    res += String((ip >> (8 * i)) & 0xFF) + ".";
  }
  res += String(((ip >> 8 * 3)) & 0xFF);
  return res;
}

void loadCredentials() {
  EEPROM.begin(512);
  EEPROM.get(0, ssid);
  EEPROM.get(0+sizeof(ssid), password);
  char ok[2+1];
  EEPROM.get(0+sizeof(ssid)+sizeof(password),espname);
  EEPROM.get(0+sizeof(ssid)+sizeof(password)+sizeof(espname),ap_name);
  EEPROM.get(0+sizeof(ssid)+sizeof(password)+sizeof(espname)+sizeof(ap_name), ap_password);
  EEPROM.get(0+sizeof(ssid)+sizeof(password)+sizeof(espname)+sizeof(ap_name)+sizeof(ap_password), ok);
  EEPROM.end();
  if (String(ok) != String("OK")) {
    ssid[0] = 0;
    password[0] = 0;
  }
    Serial.println("Recovered credentials:");
    Serial.println("SSID: [" + String(ssid) + "]" );
    Serial.println("Password: [" + String(password) + "]");    
    Serial.println("espname: [" + String(espname) + "]");
    Serial.println("ap_name: [" + String(ap_name) + "]");
    Serial.println("ap_password: [" + String(ap_password) + "]");
  
}

/** Store WLAN credentials to EEPROM */
void saveCredentials() {
  EEPROM.begin(512);
  EEPROM.put(0, ssid);
  EEPROM.put(0+sizeof(ssid), password);
  EEPROM.put(0+sizeof(ssid)+sizeof(password),espname);
  char ok[2+1] = "OK";
  EEPROM.put(0+sizeof(ssid)+sizeof(password)+sizeof(espname), ap_name);
  EEPROM.put(0+sizeof(ssid)+sizeof(password)+sizeof(espname)+sizeof(ap_name), ap_password);  
  EEPROM.put(0+sizeof(ssid)+sizeof(password)+sizeof(espname)+sizeof(ap_name)+sizeof(ap_password), ok);
  EEPROM.commit();
  EEPROM.end();
}
