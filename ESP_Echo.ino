#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <EEPROM.h>
#include <ESPAsyncWebServer.h>
#include "fauxmoESP.h"            //WeMO Emulation
fauxmoESP fauxmo;
AsyncWebServer server(80);

char versionNumber[4] = "1.6";

/*
   For the SONOFF, use 1M with 64k spiffs
   Pinout: Button-3.3v-RX-TX-GND-GPIO14
   To flash using a Wemos D1....
      Connect up the 3.3v and GND, Connect RX to RX and TX to TX. Then put a jumper on GND and RESET on the Wemos
      To program, simply hold down the button on the SONOFF and power on the Wemos, flash as normal

*/

/* Don't set these wifi credentials. They are configurated at runtime and stored on EEPROM */
char ssid[32] = "";
char password[32] = "";

//Set the default AP name/password as well as the ESP name, which will also be used as the WeMo name
char espname[32] = "SONOFF";
char ap_name[32] = "Toaster";
char ap_password[32] = "garlicbread";

#define LED_PIN              13           // GPIO 13 = Green Led (0 = On, 1 = Off) - Sonoff
#define REL_PIN              12           // GPIO 12 = Relay (0 = Off, 1 = On)
#define KEY_PIN              0            // GPIO 00 = Button
#define SONOFF_INPUT         14           //Extra GPIO, comes after GND on the programming header

//Used for LED status
#include <Ticker.h>
Ticker ticker;

//Used for the physical button press
const int CMD_WAIT = 0;
const int CMD_BUTTON_CHANGE = 1;
int cmd = CMD_WAIT;
int buttonState = HIGH; //Inverted state
static long startPress = 0;

int relayState = LOW;

int wifiTimeout = 60000;

bool restartRequired = false;


void setup() {
  //saveCredentials();
  pinMode(LED_PIN, OUTPUT);
  pinMode(REL_PIN, OUTPUT);
  pinMode(KEY_PIN, INPUT_PULLUP);  
  //  pinMode(SONOFF_BUTTON, INPUT); //GPIO pin, Pin 5 on header
  ticker.attach(0.6, tick); //Set the LED to blink when we first start up
  //setup button
  attachInterrupt(KEY_PIN, toggleState, CHANGE);

  Serial.begin(9600);
  loadCredentials(); // Load WLAN credentials from network
  connectWifi();
  serverSetup();

  // Set fauxmoESP to not create an internal TCP server and redirect requests to the server on the defined port
  // The TCP port must be 80 for gen3 devices (default is 1901)
  // This has to be done before the call to enable()
  fauxmo.createServer(false);
  fauxmo.setPort(80); // This is required for gen3 devices

  fauxmo.addDevice(espname);
  Serial.print("WeMo Emu Name: [");
  Serial.print(espname);
  Serial.println("]");
  Serial.println("Version Number");
  Serial.println(versionNumber);

  fauxmo.enable(true);
 
  fauxmo.onSetState([](unsigned char device_id, const char * device_name, bool state, unsigned char value) {
        Serial.printf("[MAIN] Device #%d (%s) state: %s value: %d\n", device_id, device_name, state ? "ON" : "OFF", value);
    if (!state)
    {
      setState(LOW);
    }
    else
    {
      setState(HIGH);
    }
  });
}

void serverSetup()
{
    // Custom entry point (not required by the library, here just as an example)
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        handleRoot(request);
    });

    // These two callbacks are required for gen1 and gen3 compatibility
    server.onRequestBody([](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
        if (fauxmo.process(request->client(), request->method() == HTTP_GET, request->url(), String((char *)data))) return;
        // Handle any other body request here...
    });
    server.onNotFound([](AsyncWebServerRequest *request) {
        String body = (request->hasParam("body", true)) ? request->getParam("body", true)->value() : String();
        if (fauxmo.process(request->client(), request->method() == HTTP_GET, request->url(), body)) return;
        returnHome(request);
       // handleNotFound;
    });

    server.on("/wifi", [](AsyncWebServerRequest *request){
          handleWifi(request);
    });
    server.on("/wifiap", [](AsyncWebServerRequest *request){
          handleWifiAP(request);
    });
    server.on("/wifisave",  [](AsyncWebServerRequest *request){
          handleWifiSave(request);
    });
    server.on("/update",  [](AsyncWebServerRequest *request){
          handleUpdate(request);
    });
    server.on("/settings",  [](AsyncWebServerRequest *request){
          handleSettings(request);
    });    
    server.on("/device_wemo",  [](AsyncWebServerRequest *request){
          handleWemo(request);
    });  
    server.on("/wemosave",  [](AsyncWebServerRequest *request){
          handleWemoSave(request);
    });   
    server.on("/remote",  [](AsyncWebServerRequest *request){
          handleArgs(request);
    });  

    server.on("/updater", HTTP_POST, [](AsyncWebServerRequest *request){
      // the request handler is triggered after the upload has finished... 
      // create the response, add header, and send response
      AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", (Update.hasError())?"FAIL":"OK");
      response->addHeader("Connection", "close");
      response->addHeader("Access-Control-Allow-Origin", "*");
      restartRequired = true;  // Tell the main loop to restart the ESP
      request->send(response);
    },[](AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final){
      //Upload handler chunks in data
    
      if(!index){ // if index == 0 then this is the first frame of data
        Serial.printf("UploadStart: %s\n", filename.c_str());
        Serial.setDebugOutput(true);        
        // calculate sketch space required for the update
        uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
        if(!Update.begin(maxSketchSpace)){//start with max available size
          Update.printError(Serial);
        }
        Update.runAsync(true); // tell the updaterClass to run in async mode
      }
      //Write chunked data to the free sketch space
      if(Update.write(data, len) != len){
          Update.printError(Serial);
      }    
      if(final){ // if the final flag is set then this is the last frame of data
        if(Update.end(true)){ //true to set the size to the current progress
            Serial.printf("Update Success: %u B\nRebooting...\n", index+len);
          } else {
            Update.printError(Serial);
          }
          Serial.setDebugOutput(false);
      }
    });

    // Start the server
    server.begin();
    Serial.println("HTTP server started");
}

//This function is called when the physical button is pressed
void toggleState() {
  cmd = CMD_BUTTON_CHANGE;
}

void connectWifi() {
  Serial.println("Setting up AP");
  WiFi.softAP(ap_name, ap_password);
  delay(1000);
  
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED && wifiTimeout != 0) {
    delay(500);
    Serial.print(".");
    wifiTimeout -= 500;
  }
  
  if(WiFi.status() == WL_CONNECTED)
  {
    Serial.println("");
    Serial.println("WiFi connected");  
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());   
    ticker.detach();
    digitalWrite(LED_PIN,HIGH);
    WiFi.softAPdisconnect(true);
  }
  else
  {
    Serial.println("WiFi not connected");
  }
}

//Used to blink the led
void tick()
{
  int state = digitalRead(LED_PIN);  // get the current state of GPIO1 pin
  digitalWrite(LED_PIN, !state);     // set pin to the opposite state
}

//Sets the relay to on or off as well as the LED
void setState(int s) {
  digitalWrite(REL_PIN, s);
  digitalWrite(LED_PIN, (s + 1) % 2); // led is active low
}


//Toggles the relay
void toggle() {
  Serial.println("toggle state");
  Serial.println(relayState);
  relayState = relayState == HIGH ? LOW : HIGH;
  setState(relayState);
}


void loop() {
  //Handle the physical button press

  switch (cmd) {
    case CMD_WAIT:
      break;
    case CMD_BUTTON_CHANGE:
      int currentState = digitalRead(KEY_PIN);
      if (currentState != buttonState) {
        if (buttonState == LOW && currentState == HIGH) {
          long duration = millis() - startPress;
          if (duration < 1000) {
            Serial.println("short press - toggle relay");
            toggle();
          } else if (duration < 5000) {
            Serial.println("medium press - restart");
            ESP.reset();
            delay(1000);
          } else if (duration < 60000) {
            Serial.println("long press - reset settings");
            //reset();
          }
        } else if (buttonState == HIGH && currentState == LOW) {
          startPress = millis();
        }
        buttonState = currentState;
      }
      break;
  }
  fauxmo.handle();
   if (restartRequired){  // check the flag here to determine if a restart is required
    Serial.printf("Restarting ESP\n\r");
    restartRequired = false;
    ESP.restart();
  }
// This is a sample code to output free heap every 5 seconds
    // This is a cheap way to detect memory leaks
    static unsigned long last = millis();
    if (millis() - last > 5000) {
        last = millis();
        Serial.printf("[MAIN] Free heap: %d bytes\n", ESP.getFreeHeap());
    }
  
}
